package pl.androidwpraktyce.szkoleniew3_preferencje;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dkrezel on 2015-05-10.
 */
public class MojeUstawienia {
	private static final String PREFFS_NAME = "moje_ustawienia";

	private static final String KEY_TEXT = "pref.text";
	private static final String KEY_CHECK = "pref.check";
	private static final String KEY_POSITION = "pref.position";

	private String mText;	//Preferencja TEXT
	private Boolean mCheck;	//Preferencja CHECK
	private Integer mPosition;	//Preferencja POSITION

	private SharedPreferences mPreferences;

	public MojeUstawienia(Context mContext) {
		// Pobranie obiektu preferencji z systemu
		// Wszystko co jest kontekstem zawiera tą metodę
		// Pobieramy zbiór preferencji o nazwie zaszytej w stałej PREFS_NAME
		mPreferences = mContext.getSharedPreferences(PREFFS_NAME, Context.MODE_PRIVATE);

		// Odświeżenia wartości przy tworzeniu obiektu
		refresh();
	}

	public MojeUstawienia refresh() {
		// Aktualizacja wartości z SharedPreferences
		this.mText = mPreferences.getString(KEY_TEXT, "");
		this.mCheck = mPreferences.getBoolean(KEY_CHECK, false);
		this.mPosition = mPreferences.getInt(KEY_POSITION, 0);

		return this;
	}

	public void save() {
		mPreferences.edit()
				.putString(KEY_TEXT, mText)
				.putBoolean(KEY_CHECK, mCheck)
				.putInt(KEY_POSITION, mPosition)
				.commit();	// Trzeba pamiętać o zapisaniu tych wartości po edycji !!
	}

	public String getText() {
		return mText;
	}

	public MojeUstawienia setText(String mText) {
		this.mText = mText;
		return this;
	}

	public Boolean getCheck() {
		return mCheck;
	}

	public MojeUstawienia setCheck(Boolean mCheck) {
		this.mCheck = mCheck;
		return this;
	}

	public Integer getPosition() {
		return mPosition;
	}

	public MojeUstawienia setPosition(Integer mPosition) {
		this.mPosition = mPosition;
		return this;
	}
}
