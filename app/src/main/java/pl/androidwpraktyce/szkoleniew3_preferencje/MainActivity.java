package pl.androidwpraktyce.szkoleniew3_preferencje;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;


public class MainActivity extends ActionBarActivity {


	private EditText mText;
	private CheckBox mCheckBox;
	private RadioGroup mRadioGroup;
	private RadioButton mRadio1, mRadio2;
	private Button mSave;

	private MojeUstawienia mUstawienia;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mText = findView(R.id.ustawienie_text);
		mCheckBox = findView(R.id.ustawienie_checkbox);
		mRadioGroup = findView(R.id.ustawienie_radio);
		mRadio1 = findView(R.id.ustawienie_radio1);
		mRadio2 = findView(R.id.ustawienie_radio2);
		mSave = findView(R.id.button_zapisz);

		mUstawienia = new MojeUstawienia(this);
		implementButtonListener();

		// Jeżeli przekręcamy ekran to niech odtworzy się to co było wpisane
		// Przy pierwszym uruchomieniu tego ekrenu niech wczyta dane z preferencji
		if (savedInstanceState == null) {
			readPreferences();
		}
	}

	// Załadowanie danych z klasy MojeUstawienia do widoku formularza
	protected void readPreferences() {
		mText.setText(mUstawienia.getText());
		mCheckBox.setChecked(mUstawienia.getCheck());
		mRadioGroup.check(mUstawienia.getPosition());

	}

	public void implementButtonListener() {
		mSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAndSavePreferences();
			}
		});
	}

	private void setAndSavePreferences() {
		mUstawienia.setText(mText.getText().toString());
		mUstawienia.setCheck(mCheckBox.isChecked());
		mUstawienia.setPosition(mRadioGroup.getCheckedRadioButtonId());
		mUstawienia.save();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private <T> T findView(int viewId) {
		return (T) findViewById(viewId);
	}
}
